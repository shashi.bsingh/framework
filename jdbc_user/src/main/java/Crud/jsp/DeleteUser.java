package Crud.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DeleteUser {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		Statement st = con.createStatement();
		int nor = st.executeUpdate("delete from user where id = 2 ");
		st.close();
		con.close();
		System.out.println(nor);
	}
}