package com.jsp.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.SimCard;
import com.jsp.dto.SmartPhone;

public class TestSave {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		SmartPhone phone = new SmartPhone();
		phone.setRam(4);
		phone.setStorage(64);
		phone.setBrand("VIVO");

		SimCard sim1 = new SimCard();
		sim1.setNumber(767767);
		sim1.setService_provider("JIO");
		sim1.setType("Prepaid");
		sim1.setSmartPhone(phone);

		SimCard sim2 = new SimCard();
		sim2.setNumber(99999);
		sim2.setService_provider("AIRTEL");
		sim2.setType("Postpaid");
		sim2.setSmartPhone(phone);

		transaction.begin();
		manager.persist(sim1);
		manager.persist(sim2);
		manager.persist(phone);
		transaction.commit();

	}

}
