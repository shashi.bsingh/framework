package com.jsp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Branch;
import com.jsp.dto.Hospital;

public class HospitalController {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Hospital hospital = new Hospital();
		hospital.setName("Apollo");
		hospital.setFounder("ABC");
		hospital.setGst("AP123");

		List<Branch> branches = new ArrayList<Branch>();
		Branch b1 = new Branch();
		b1.setEmail("1@gmail.com");
		b1.setName("banglore");
		b1.setPhone(8889);

		branches.add(b1);

		Branch b2 = new Branch();
		b2.setEmail("2@gmail.com");
		b2.setName("manglore");
		b2.setPhone(8889878);

		branches.add(b2);
		hospital.setBranches(branches);

		transaction.begin();
		manager.persist(b1);
		manager.persist(b2);
		manager.persist(hospital);
		transaction.commit();

	}
}
