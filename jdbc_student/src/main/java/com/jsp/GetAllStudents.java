package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GetAllStudents {
	public static void getAllStudents() throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_student", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from student");
		// statement.setInt(1, id);
		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(res.getInt(1) + "  " + res.getString(2) + " " + res.getString(3) + "        "
					+ res.getInt(4) + " ");
		}

	}

	public static void main(String[] args) throws Exception {
		getAllStudents();
	}
}
