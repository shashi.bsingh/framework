package com.jsp.controller;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.jsp.dto.User;

public class UserLogin {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("enter phone number and password to login");
		long phone = s.nextLong();
		String password = s.next();
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		Query q = manager.createQuery("select u from User u where u.phone=?1 and u.password=?2");
		q.setParameter(1, phone);
		q.setParameter(2, password);
		List<User> users = q.getResultList();
		if (users.size() > 0) {
			System.out.println("Login is successful and your results are below");
			User u = users.get(0);
			System.out.println(u.getName());
			System.out.println(u.getPassword());
			System.out.println(u.getPhone());
			System.out.println(u.getId());
		} else {
			System.out.println("invalid phone number or password");
		}

	}

}
