package com.jsp.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Student;

public class StudentController {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Student s = new Student();
		s.setId(5);
		s.setName("shashi");
		s.setPhone(67895);
		s.setUsn(989);
		transaction.begin();
		manager.persist(s);
		transaction.commit();

	}

}