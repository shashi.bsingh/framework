package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class InsertStudent {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		// Establish the connection
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_demo", "root", "root");
		// Create the statement
		Statement st = con.createStatement();
		// ecexcute the query
//		st.execute("insert into student values(2,'dinga',67888)");
		// st.execute("insert into student values(5,'dongp',67988)");
		// st.execute("insert into student values(6,'domp',64567)");
//		st.execute("insert into student values(7,'domp',64567)");
		st.execute("insert into student values(8,'momp',69567)");
		st.execute("insert into student values(9,'komp',69467)");
		st.execute("insert into student values(10,'lomp',689567)");
		st.execute("insert into student values(11,'tomp',69067)");
		// close the connection
		st.close();
		con.close();
	}
}
