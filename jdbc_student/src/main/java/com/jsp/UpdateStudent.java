package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class UpdateStudent {
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter id of the student to update");
		int id = s.nextInt();
		System.out.println("Enter the name and age of the student ");
		String name = s.next();

		int age = s.nextInt();
		Student student = new Student();
		student.setId(id);
		student.setAge(age);
		student.setName(name);

		updateStudent(student);

	}

	public static void updateStudent(Student student) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_student", "root", "root");
		PreparedStatement statement = connection.prepareStatement("update student set name=?, age=? where id =?");
		statement.setString(1, student.getName());
		statement.setInt(2, student.getAge());
		statement.setInt(3, student.getId());
		int nor = statement.executeUpdate();
		statement.close();
		connection.close();
		System.out.println(nor + "rows updated");
	}
}
