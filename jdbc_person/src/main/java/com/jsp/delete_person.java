package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class delete_person {

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the id to delete");
		int id = s.nextInt();
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_person", "root", "root");
		PreparedStatement st = con.prepareStatement("delete from person where id=?");
		st.setInt(1, id);
		int nor = st.executeUpdate();
		st.close();
		con.close();
		System.out.println(nor + "rows deleted");
	}

}
