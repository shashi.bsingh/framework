package com.jsp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class InserUser {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		User user = new User();
		user.setId(3);
		user.setName("shashi");
		user.setPhone(67895);
		user.setEmail("d@mohan.com");
		transaction.begin();
		manager.persist(user);
		transaction.commit();

	}

}
