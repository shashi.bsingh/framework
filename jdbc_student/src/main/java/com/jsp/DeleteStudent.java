package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class DeleteStudent {
	public static void deleteStudents(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_student", "root", "root");
		PreparedStatement statement = connection.prepareStatement("delete from student where id = ?");
		statement.setInt(1, id);
//		ResultSet res = statement.executeQuery();
		int r = statement.executeUpdate();
		System.out.println(r + " Rows deleted ");

	}

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the id to delete ");
		int id = s.nextInt();

		deleteStudents(id);

	}
}
