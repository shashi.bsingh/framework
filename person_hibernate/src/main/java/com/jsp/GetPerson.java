package com.jsp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GetPerson {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Person p = manager.find(Person.class, 1);
		if (p != null) {

			System.out.print(p.getId() + "   ");
			System.out.print(p.getAge() + "   ");
			System.out.print(p.getName() + "   ");
			System.out.print(p.getPhone() + "   ");
		} else {
			System.out.println("person with given id doesnot exist");
		}

	}

}
