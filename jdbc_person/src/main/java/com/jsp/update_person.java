package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class update_person {

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the id of person to update");
		int id = s.nextInt();
		System.out.println("Enter the new name and phone");
		String name = s.next();
		long phone = s.nextLong();
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_person", "root", "root");
		PreparedStatement st = con.prepareStatement("update person set name=? ,phone=? where id=?");
		st.setString(1, name);
		st.setLong(2, phone);
		st.setInt(3, id);
		int nor = st.executeUpdate();
		st.close();
		con.close();
		System.out.println(nor + "rows updated");

	}

}
