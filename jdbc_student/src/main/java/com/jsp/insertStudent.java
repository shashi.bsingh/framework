package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class insertStudent {
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter id,name, usn and age");
		int id = s.nextInt();
		String name = s.next();
		String usn = s.next();
		int age = s.nextInt();
		Student student = new Student();
		student.setAge(age);
		student.setId(id);
		student.setName(name);
		student.setUsn(usn);
		insertStudent(student);

	}

	static public void insertStudent(Student student) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_student", "root", "root");
		PreparedStatement statement = connection.prepareStatement("insert into student values(?,?,?,?)");
		statement.setInt(1, student.getId());
		statement.setString(2, student.getName());
		statement.setString(3, student.getUsn());
		statement.setInt(4, student.getAge());

		statement.execute();
		statement.close();
		connection.close();
	}
}