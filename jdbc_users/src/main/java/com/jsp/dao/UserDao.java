package com.jsp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDao {

	public void insertUser(User user) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/users_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("insert into user values(?,?,?,?,?)");
		statement.setInt(1, user.getId());
		statement.setString(2, user.getName());
		statement.setInt(3, user.getPhone());
		statement.setInt(4, user.getAge());
		statement.setInt(5, user.getPassword());
		boolean nor = statement.execute();
		statement.close();
		connection.close();
		System.out.println("rows updated");
	}

	public void updateUser(User user) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		PreparedStatement statement = connection
				.prepareStatement("update user set name=?, phone=?,age=? ,password =? where id =?");
		statement.setString(1, user.getName());
		statement.setInt(2, user.getPhone());
		statement.setInt(3, user.getAge());
		statement.setInt(4, user.getPassword());
		statement.setInt(5, user.getId());
		int nor = statement.executeUpdate();
		statement.close();
		connection.close();
		System.out.println(nor + "rows updated");
	}

	public void getAllUsers() throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from user");

		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(res.getInt(1) + "  " + res.getString(2) + " " + res.getDouble(3) + "        "
					+ res.getString(4) + " ");
		}

	}

	public void getUser(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from user where id =?");
		statement.setInt(1, id);
		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(
					res.getInt(1) + "  " + res.getString(2) + " " + res.getDouble(3) + " " + res.getString(4) + " ");
		}

	}

	public void deleteUser(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("delete from user where id = ?");
		statement.setInt(1, id);
		int r = statement.executeUpdate();
		System.out.println(r + " Rows deleted ");

	}

}
