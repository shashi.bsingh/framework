package com.jsp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class InsertPerson {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Person person = new Person();
		person.setId(3);
		person.setName("shashi");
		person.setAge(67);
		person.setPhone(766788979770l);
		transaction.begin();
		manager.persist(person);
		transaction.commit();

	}

}
