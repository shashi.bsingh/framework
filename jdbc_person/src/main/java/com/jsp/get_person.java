package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class get_person {

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the id");
		int id = s.nextInt();
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_person", "root", "root");
		// Statement st = con.createStatement();
		PreparedStatement st = con.prepareStatement("select * from person where id =?");
		st.setInt(1, id);
		ResultSet res = st.executeQuery();
		while (res.next()) {
			System.out.println(res.getInt(1) + "  " + res.getString(2) + "  " + res.getInt(3) + " ");
		}

	}
}