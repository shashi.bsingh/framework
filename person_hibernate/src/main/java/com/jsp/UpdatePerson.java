package com.jsp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class UpdatePerson {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Person p = new Person();
		p.setId(5);
		p.setName("shashi");
		p.setAge(67);
		p.setPhone(766788979770l);

		transaction.begin();
		manager.merge(p);
		transaction.commit();
	}

}
