package com.jsp.dto;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class AddCar {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Car car = new Car();
		car.setBrand("swift");
		car.setColor("Blue");
		Engine e = new Engine();
		e.setNoc(4);
		e.setCc(400);
		car.setEngine(e);
		transaction.begin();
		manager.persist(e);
		manager.persist(car);
		transaction.commit();

	}

}
