package com.jsp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DeletePerson {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Person p = manager.find(Person.class, 4);
		if (p != null) {

			transaction.begin();
			manager.remove(p);
			transaction.commit();
			System.out.println("person deleted succesfully");
		} else {
			System.out.println("primary key doesnot exist");
		}
	}

}
