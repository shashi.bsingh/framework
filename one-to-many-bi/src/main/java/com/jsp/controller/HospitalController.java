package com.jsp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Branch;
import com.jsp.dto.Hospital;

public class HospitalController {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("shashi");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Hospital hospital = new Hospital();
		hospital.setFounder("ABC");
		hospital.setGst("ABC234");
		hospital.setName("Apollo");

		Branch branch1 = new Branch();
		branch1.setEmail("ab@gmail.com");
		branch1.setName("Banglore Appolo");
		branch1.setPhone(6577678687L);

		Branch branch2 = new Branch();
		branch2.setEmail("po@gmail.com");
		branch2.setName("Manglore Appolo");
		branch2.setPhone(99898987L);

		List<Branch> branches = new ArrayList<Branch>();

		branches.add(branch1);
		branches.add(branch2);

		hospital.setBranches(branches);
		branch1.setHospital(hospital);
		branch2.setHospital(hospital);

		transaction.begin();
		manager.persist(branch1);
		manager.persist(branch2);
		manager.persist(hospital);
		transaction.commit();

	}

}
