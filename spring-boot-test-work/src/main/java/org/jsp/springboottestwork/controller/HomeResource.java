package org.jsp.springboottestwork.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResource {
	@GetMapping("/get")
	public String get() {
		return "Welcome to spring Boot Applicatiom";
	}
	
	@GetMapping("/getSum")
	public String getSum(@RequestParam int a ,@RequestParam int b) {
	return "the sum is : +(a+b) ";	
	}
}
