package com.jsp.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.jsp.dto.Branch;
import com.jsp.dto.Hospital;

public class TestgetHospital {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		Hospital hospital = manager.find(Hospital.class, 2);
		System.out.println(hospital.getFounder());
		System.out.println(hospital.getId());
		System.out.println(hospital.getGst());
		System.out.println(hospital.getName());
		List<Branch> branches = hospital.getBranches();
		for (Branch b : branches) {
			System.out.println(b.getEmail());
			System.out.println(b.getId());
			System.out.println(b.getPhone());
			System.out.println(b.getName());
		}

	}

}
