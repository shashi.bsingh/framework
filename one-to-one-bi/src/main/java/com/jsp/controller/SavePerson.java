
package com.jsp.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Pan;
import com.jsp.dto.Person;

public class SavePerson {
	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Person person = new Person();
		person.setAge(23);
		person.setGender("male");
		person.setName("dinga");

		Pan pan = new Pan();
		pan.setCountry("India");
		pan.setNumber("FGFHH9879");
		pan.setPhone(7878787878L);

		pan.setPerson(person);
		person.setPan(pan);

		transaction.begin();
		manager.persist(pan);
		manager.persist(person);
		transaction.commit();

	}

}