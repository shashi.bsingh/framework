package com.jsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeResource {
	@RequestMapping("/get")
	public ModelAndView get() {
		ModelAndView view = new ModelAndView();
		view.addObject("name", "Hello from Spring MVC");
		view.addObject("message", "This is the MVC project ");
		view.setViewName("demo.jsp");
		return view;
	}
}
