package com.jsp.controller;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Car;
import com.jsp.dto.Engine;

public class TestSave {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();
		Car car = new Car();
		car.setBrand("Jaguar");
		car.setPrice(12323223);

		Engine e = new Engine();
		e.setNoc(4);
		e.setCc(700);
		car.setEngine(e);
		e.setCar(car);
		transaction.begin();
		manager.persist(e);
		manager.persist(car);
		transaction.commit();

	}

}
