package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class GetUser {

	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		// Establish the connection
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_jdbc", "root", "root");
		// Create the statement
		Statement st = con.createStatement();
		ResultSet res = st.executeQuery("select * from user");
		while (res.next()) {
			System.out.println(res.getInt(1) + "  " + res.getString(2) + "  " + res.getLong(3) + " " + res.getString(4)
					+ " " + res.getString(5));
		}

	}
}
