package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class Inser_person {
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter id,name and phone");
		int id = s.nextInt();
		String name = s.next();
		long phone = s.nextLong();
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_person", "root", "root");
		PreparedStatement st = con.prepareStatement("insert into person values(?,?,?)");
		st.setInt(1, id);
		st.setString(2, name);
		st.setLong(3, phone);
		st.execute();
		st.close();
		con.close();
		System.out.println("Data Inserted Succesfully");
	}
}
