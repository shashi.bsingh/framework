package com.jsp.controller;

import java.util.Scanner;

import com.jsp.dao.CarDao;
import com.jsp.dto.Car;

public class InsertCar {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		boolean exit = true;

		while (exit) {
			System.out.println();
			System.out.println("Enter your Operation: ");
			System.out.println(" 1- Insert the car data into the database");
			System.out.println(" 2- Update the car data into the database");
			System.out.println(" 3- Get the all car data from the database");
			System.out.println(" 4- Get the single car data from the database corresponding the car id");
			System.out.println(" 5- Delete the car data from the database corresponding the car id");
			System.out.println(" 6- Exit the Car opeartion with database");
			System.out.println("Enter your choice");
			int choice = sc.nextInt();
			switch (choice) {
			case 1: {
				Scanner s = new Scanner(System.in);
				System.out.println("Enter id , brand ,price ,and color to insert ");
				int id = s.nextInt();
				String brand = s.next();
				double price = s.nextDouble();
				String color = s.next();
				Car car = new Car();
				car.setId(id);
				car.setBrand(brand);
				car.setPrice(price);
				car.setColor(color);
				CarDao dao = new CarDao();
				dao.insertCar(car);
			}
				break;

			case 2: {
				Scanner s = new Scanner(System.in);
				System.out.println("Enter id of the car to update");
				int id = s.nextInt();
				System.out.println("Enter the brand , price and color of the car ");
				String brand = s.next();
				double price = s.nextDouble();
				String color = s.next();
				Car car = new Car();
				car.setId(id);
				car.setBrand(brand);
				car.setPrice(price);
				car.setColor(color);
				CarDao dao = new CarDao();
				dao.updateCar(car);

			}
				break;

			case 3: {
				CarDao dao = new CarDao();
				dao.getAllCars();
			}
				break;

			case 4: {
				Scanner s = new Scanner(System.in);
				System.out.println("Enter the Id to get");
				int id = s.nextInt();
				CarDao dao = new CarDao();
				dao.getCar(id);

			}
				break;
			case 5: {
				Scanner s = new Scanner(System.in);
				System.out.println("Enter the id to delete ");
				int id = s.nextInt();
				CarDao dao = new CarDao();
				dao.deleteCar(id);

			}
				break;

			case 6: {
				exit = false;
			}
				break;

			default: {
				System.out.println("enter the valid choice");
			}
				break;
			}
		}

		// 1 - insert the car data into the database
		/*
		 * Scanner s = new Scanner(System.in);
		 * System.out.println("Enter id , brand ,price ,and color to insert "); int id =
		 * s.nextInt(); String brand = s.next(); double price = s.nextDouble(); String
		 * color = s.next(); Car car = new Car(); car.setId(id); car.setBrand(brand);
		 * car.setPrice(price); car.setColor(color); CarDao dao = new CarDao();
		 * dao.insertCar(car);
		 */
		// 2 - update the car data into the database
		/*
		 * Scanner s = new Scanner(System.in);
		 * System.out.println("Enter id of the car to update"); int id = s.nextInt();
		 * System.out.println("Enter the brand , price and color of the car "); String
		 * brand = s.next(); double price = s.nextDouble(); String color = s.next(); Car
		 * car = new Car(); car.setId(id); car.setBrand(brand); car.setPrice(price);
		 * car.setColor(color); CarDao dao = new CarDao(); dao.updateCar(car);
		 */
		// 3 - Get the all car data from the database

		// CarDao dao = new CarDao();
		// dao.getAllCars();
		// 4 - Get the single car data from the database corresponding the car id

		/*
		 * Scanner s = new Scanner(System.in);
		 * System.out.println("Enter the Id to get"); int id = s.nextInt(); CarDao dao =
		 * new CarDao(); dao.getCar(id);
		 */
		// 5 - delete the car data from the database corresponding the car id

		/*
		 * Scanner s = new Scanner(System.in);
		 * System.out.println("Enter the id to delete "); int id = s.nextInt(); CarDao
		 * dao = new CarDao(); dao.deleteCar(id);
		 */
	}
}
