package com.jsp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.jsp.dto.Car;

public class CarDao {

	public void insertCar(Car car) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/car_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("insert into car values(?,?,?,?)");
		statement.setInt(1, car.getId());
		statement.setString(2, car.getBrand());
		statement.setDouble(3, car.getPrice());
		statement.setString(4, car.getColor());
		boolean nor = statement.execute();
		statement.close();
		connection.close();
		System.out.println("rows updated");
	}

	public void updateCar(Car car) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/car_jdbc", "root", "root");
		PreparedStatement statement = connection
				.prepareStatement("update car set brand=?, price=?,color=? where id =?");
		statement.setString(1, car.getBrand());
		statement.setDouble(2, car.getPrice());
		statement.setString(3, car.getColor());
		statement.setInt(4, car.getId());
		int nor = statement.executeUpdate();
		statement.close();
		connection.close();
		System.out.println(nor + "rows updated");
	}

	public void getAllCars() throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/car_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from car");

		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(res.getInt(1) + "  " + res.getString(2) + " " + res.getDouble(3) + "        "
					+ res.getString(4) + " ");
		}

	}

	public void getCar(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/car_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from car where id =?");
		statement.setInt(1, id);
		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(
					res.getInt(1) + "  " + res.getString(2) + " " + res.getDouble(3) + " " + res.getString(4) + " ");
		}

	}

	public void deleteCar(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/car_jdbc", "root", "root");
		PreparedStatement statement = connection.prepareStatement("delete from car where id = ?");
		statement.setInt(1, id);
		int r = statement.executeUpdate();
		System.out.println(r + " Rows deleted ");

	}

}
