package com.jsp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class GetAllPersons {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();

		Query q = manager.createQuery("select p from Person p");
		List<Person> persons = q.getResultList();
		for (Person p : persons) {
			System.out.println(p.getId());
			System.out.println(p.getAge());
			System.out.println(p.getName());
			System.out.println(p.getPhone());
			System.out.println("**********************");
		}

	}

}
