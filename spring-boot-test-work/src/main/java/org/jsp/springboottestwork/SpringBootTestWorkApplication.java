package org.jsp.springboottestwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTestWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTestWorkApplication.class, args);
	}

}
