package com.jsp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.jsp.dto.Course;
import com.jsp.dto.Student;

public class Test {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("sathish");
		EntityManager manager = factory.createEntityManager();
		EntityTransaction transaction = manager.getTransaction();

		Student s1 = new Student();
		s1.setAge(34);
		s1.setName("Mohan");

		Student s2 = new Student();
		s2.setAge(45);
		s2.setName("Rohan");

		Course c1 = new Course();
		c1.setSubjects("Java");
		c1.setDuration(56);

		Course c2 = new Course();
		c2.setSubjects("Python");
		c2.setDuration(45);

		Course c3 = new Course();
		c3.setSubjects("Sql");
		c3.setDuration(40);

		List<Course> forS1 = new ArrayList<Course>();
		forS1.add(c1);
		forS1.add(c3);

		List<Course> forS2 = new ArrayList<Course>();
		forS2.add(c1);
		forS2.add(c2);
		forS2.add(c3);

		List<Student> forC1 = new ArrayList<Student>();
		forC1.add(s1);
		forC1.add(s2);

		List<Student> forC2 = new ArrayList<Student>();

		forC2.add(s2);

		List<Student> forC3 = new ArrayList<Student>();
		forC3.add(s1);
		forC3.add(s2);

		s1.setCourses(forS1);
		s2.setCourses(forS2);

		c1.setStudents(forC1);
		c2.setStudents(forC2);
		c3.setStudents(forC3);
		transaction.begin();
		manager.persist(c1);
		manager.persist(c2);
		manager.persist(c3);
		manager.persist(s1);
		manager.persist(s2);

		transaction.commit();

	}

}
