package com.jsp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class GetStudent {
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the Id to get");
		int id = s.nextInt();
		getStudent(id);

	}

	public static void getStudent(int id) throws Exception {

		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_student", "root", "root");
		PreparedStatement statement = connection.prepareStatement("select * from student where id =?");
		statement.setInt(1, id);
		ResultSet res = statement.executeQuery();
		while (res.next()) {
			System.out.println(
					res.getInt(1) + "  " + res.getString(2) + " " + res.getString(3) + " " + res.getInt(4) + " ");
		}

	}
}
